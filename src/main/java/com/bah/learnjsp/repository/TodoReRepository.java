package com.bah.learnjsp.repository;

import com.bah.learnjsp.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoReRepository extends JpaRepository <Todo, Long> {

    List<Todo> findByUserName(String user);
}
